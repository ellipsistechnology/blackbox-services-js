export interface ServiceType {
  uri:string
  name:string
  format?:string
  description?:string
}

export interface ServiceLink extends Link {
  types?:ServiceType[]
}

export interface ServiceLinks {
  [key:string]:ServiceLink;
}

export interface Service {
  description:string
  bbversion:string
  links:ServiceLinks
}

export interface NamedReference {
  name:string
}

export interface Link {
  href:string
  description?:string
  [key:string]:Link|any
}

export interface Links {
  self:Link;
  [key:string]:Link;
}

export interface VerboseObject {
  links?:Links
}

export function nameOf(data:any):NamedReference {
  return <NamedReference>{
    name: data.__id ? data[data.__id] : data.name,
    __path: data.__path
  }
}

function convertBooleanParameter(val:any):boolean {
  if(val === 'false')
       return false;
  if(val === '')
     return true;
  return val
}

export function verbose({links}:{links: {[key:string]:Link}}) {
  return function(target: any) {
    if(!links)
      throw new Error(`@verbose decorator is missing links`)
    if(typeof target === 'function' && target.prototype)
      target.prototype.__links = links
    else
      target.__links = links
  }
}

export function verboseResponse<T>(target:T|T[], verbose:boolean = false):(VerboseObject&T)|(VerboseObject&T)[] {
  if(Array.isArray(target))
    return verboseResponseArray(target, verbose)
  else
    return verboseResponseObject(target, verbose)
}

export function verboseResponseObject<T>(target:T, verbose:boolean = false):(VerboseObject&T) {
  verbose = convertBooleanParameter(verbose)
  if(verbose && (<any>target).__path) {
    const v = Object.assign({ links: { self: { href: `${(<any>target).__path}/${(<any>target).__id ? target[(<any>target).__id] : (<any>target).name}` } } }, target)
    if((<any>target).__links) {
      Object.assign(v.links, (<any>target).__links)
      delete (<any>v).__links
    }
    return v
  }
  else {
    return target as (VerboseObject&T)
  }
}

export function verboseResponseArray<T>(targets:T[], verbose:boolean = false):(VerboseObject&T)[] {
  verbose = convertBooleanParameter(verbose)
  if(!verbose)
    return targets as (VerboseObject&T)[]

  return targets.map( (target) => verboseResponse(target, true) ) as (VerboseObject&T)[]
}

export function trimToDepthArray(entity:any[], depth:number = 0):any[] {
  return entity.map( e => trimToDepth(e, depth) )
}

export function trimToDepth(entity:any, depth:number = 0):any {

  if(!entity || typeof entity !== 'object')
    return entity

  if(Array.isArray(entity))
    return trimToDepthArray(entity, depth)

  let trimmed = {}
  const id = entity.__id ? entity.__id : 'name'

  if(!entity)
    return entity

  Object.keys(entity).forEach(key => {
    // If the property is undefined or not hierarchical then just copy it:
    if(!entity[key] || !entity[key].__path) {
      trimmed[key] = entity[key]
    }
    // Recursively copy up to depth:
    else {
      if(!key.startsWith('__')) { // ignore config items starting with __
        // At max depth then insert link only:
        if(depth === 0) {
          trimmed[key] = {href: `${entity[key].__path}/${entity[key][id]}`}
        }
        // Recurse on child hierarchy:
        else {
          trimmed[key] = trimToDepth(entity[key], depth-1)
        }
      }
    }
  })

  return trimmed
}

export function identifiedBy({id}:{id:string}) {
  return function(target: any) {
    if(!id)
      throw new Error(`@identifiedBy decorator is missing id`)
    if(typeof target === 'function' && target.prototype)
      target.prototype.__id = id
    else
      target.__id = id
  }
}

export function hierarchical({path}:{path:string}) {
  return function(target: any) {
    if(!path)
      throw new Error(`@hierarchical decorator is missing path for target ${target.__id ? target[target.__id] : target.name}`)
    if(typeof target === 'function' && target.prototype)
      target.prototype.__path = path
    else
      target.__path = path
  }
}

export function hasChildren(oasDoc:any, path:string):boolean {
  const matcher = new RegExp(`^${path}\/.+`)
  for(let p of (Object.keys(oasDoc.paths))) {
    if(matcher.test(p))
      return true
  }
  return false
}

export function findParentPaths(oasDoc:any):string[] {
  return Object.keys(oasDoc.paths)
    .filter((path:string) => /^\/[^\/]+$/.test(path)) // only one path component
    // .filter((path:string) => hasChildren(oasDoc, path)) // must be a parent
}

export function findChildren(oasDoc:any, rootServiceName:string):string[] {

  if(rootServiceName.startsWith('/'))
    rootServiceName = rootServiceName.substring(1)

  const matcher = rootServiceName === '' ? new RegExp(`^\/[^\/]+$`) : new RegExp(`^\/${rootServiceName}\/[^\/]+$`)
  return Object.keys(oasDoc.paths)
    .map( (path:string) => path.endsWith('/') ? path.substring(0, path.length-1) : path ) // strip trailing slash
    .filter( (path:string) => matcher.test(path) )
}

export function makeServiceObject(oasDoc:any, rootServiceName:string):Service {

  if(rootServiceName.startsWith('/'))
    rootServiceName = rootServiceName.substring(1)

  const self = "/"+rootServiceName
  if(!oasDoc.paths[self])
    throw new Error(`Path ${rootServiceName} not found.`)

  // Find immediate children:
  const children:string[] = findChildren(oasDoc, rootServiceName)
  if(children.length === 0)
    return undefined

  // Create service object:
  const service = {
    description:oasDoc.paths["/"+rootServiceName] ? oasDoc.paths[self].get.summary : "",
    bbversion:"0.0.1",
    links: { self: {href: self} }
  }

  // Add links:
  children.forEach((child:string) => {
    const name = /[^\/]+$/.exec(child)[0]
    let types = oasDoc.paths[child]['x-blackbox-types']
    if(!types || types.length === 0) {
      types = findChildren(oasDoc, name).reduce( (prev:[], current) => {
        return oasDoc.paths[current]['x-blackbox-types'] ?
          prev.concat(oasDoc.paths[current]['x-blackbox-types']) : prev
      }, [])
    }
    service.links[name] = {
      href: child,
      description: oasDoc.paths[child].get.summary,
      types: types
    }
  })

  return service
}
