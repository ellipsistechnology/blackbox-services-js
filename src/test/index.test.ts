import { verboseResponse, verboseResponseArray, trimToDepth, hierarchical, nameOf, makeServiceObject, findParentPaths, findChildren, verbose, verboseResponseObject } from "..";
import fs = require('fs');
import path = require('path');

test("Verbose response includes verbose object only when verbose is true", () => {
  const data:any = {
    __path: "/self/path",
    test: "some test data",
    name: "test"
  }
  const newData1 = verboseResponse(data, undefined)
  expect(newData1).toBe(data)
  expect(newData1.links).toBeUndefined()
  const newData2 = verboseResponse(data, false)
  expect(newData2).toBe(data)
  expect(newData2.links).toBeUndefined()
  const newData3 = verboseResponse(data, <boolean><any>'false')
  expect(newData3).toBe(data)
  expect(newData3.links).toBeUndefined()

  const newData4 = verboseResponse(data, true)
  expect(newData4.links).toBeDefined()
  expect(newData4.test).toBe(data.test)
  const newData5 = verboseResponse(data, <boolean><any>'true')
  expect(newData5.links).toBeDefined()
  expect(newData5.test).toBe(data.test)
  const newData6 = verboseResponse(data, <boolean><any>'')
  expect(newData6.links).toBeDefined()
  expect(newData6.test).toBe(data.test)
})

test("Verbose response includes correct path.", () => {
  const data:any = {
    __path: "/self/path",
    test: "some test data",
    name: "test"
  }
  const newData = verboseResponse(data, true)
  expect(newData.links).toBeDefined()
  expect(newData.links.self).toBeDefined()
  expect(newData.links.self.href).toEqual('/self/path/test')
  expect(newData.test).toBe(data.test)
})

test("Verbose response operates on all elements of an array.", () => {
  const data = [
    {__path: "/self/path", name: "test1", test: "some test data"},
    {__path: "/self/path", name: "test2", test: 0},
    {__path: "/self/path", name: "test3", test: false},
    {__path: "/self/path", name: "test4", test: "some ther test data"}
  ]

  const newData1 = verboseResponseArray(data, undefined)
  newData1.forEach((d, i) => expect(d).toBe(data[i]))
  newData1.forEach(d => expect(d.links).toBeUndefined())
  const newData2 = verboseResponseArray(data, false)
  newData2.forEach((d, i) => expect(d).toBe(data[i]))
  newData2.forEach(d => expect(d.links).toBeUndefined())
  const newData3 = verboseResponseArray(data, <boolean><any>'false')
  newData3.forEach((d, i) => expect(d).toBe(data[i]))
  newData3.forEach(d => expect(d.links).toBeUndefined())

  const newData4 = verboseResponseArray(data, true)
  newData4.forEach(d => expect(d.links).toBeDefined())
  newData4.forEach((d, i) => expect(d.test).toBe(data[i].test))
  const newData5 = verboseResponseArray(data, <boolean><any>'true')
  newData5.forEach(d => expect(d.links).toBeDefined())
  newData5.forEach((d, i) => expect(d.test).toBe(data[i].test))
  const newData6 = verboseResponseArray(data, <boolean><any>'')
  newData6.forEach(d => expect(d.links).toBeDefined())
  newData6.forEach((d, i) => expect(d.test).toBe(data[i].test))
})

test("@verbose adds a link to verboseResponse.", () => {
  @verbose({links: {
    testLink: {
      href: "/some/path",
      description: "some description"
    }
  }})
  @hierarchical({path: "/some/path"})
  class Data {}

  const data = new Data();
  const verboseData = verboseResponseObject(data, true)
  expect(verboseData).toBeDefined()
  expect(verboseData.links).toBeDefined()
  expect(verboseData.links.testLink).toEqual({
    href: "/some/path",
    description: "some description"
  })
  expect(verboseData.links.self).toBeDefined()
})

function trimData():any {
  return {
    name: "root",
    item1: {
      __path: '/root/item1s',
      name: "item 1",
      item2: {
        __path: '/root/items',
        name: "item 2",
        item3: {
          __path: '/root/items',
          name: "item 3",
          item4: {
            __path: '/root/items',
            name: "item 4"
          }
        }
      },
      nullItem: null,
      undefinedItem: undefined,
      falseItem: false,
      trueItem: true,
      numberItem: 1.2
    },
    item5: {
      __path: '/root/item5s',
      name: "item 5",
      item2: {
        __path: '/root/items',
        name: "item 2",
        item3: undefined
      }
    },
    item6: { text: "Not part of the hierarchy" }
  }
}

function testDepth0(data: any) {
    const trimmed = trimToDepth(data, 0);
    testDepth0TrimmedData(trimmed, data);
}

function testDepth0TrimmedData(trimmed: any, data: any) {
    expect(trimmed.item6.text).toBe(data.item6.text);
    expect(Object.keys(trimmed.item1).length).toEqual(1);
    expect(trimmed.item1.href).toEqual(`/root/item1s/${data.item1.name}`);
    expect(Object.keys(trimmed.item5).length).toEqual(1);
    expect(trimmed.item5.href).toEqual(`/root/item5s/${data.item5.name}`);
}

function testDepth1(data: any) {
    const trimmed = trimToDepth(data, 1);
    expect(trimmed.item6.text).toBe(data.item6.text);
    expect(trimmed.item1).toBeDefined();
    expect(trimmed.item1.name).toEqual(data.item1.name);
    expect(Object.keys(trimmed.item1.item2).length).toEqual(1);
    expect(trimmed.item1.item2.href).toEqual(`/root/items/${data.item1.item2.name}`);
    expect(Object.keys(trimmed.item5.item2).length).toEqual(1);
    expect(trimmed.item5.item2.href).toEqual(`/root/items/${data.item5.item2.name}`);
}

function testDepth2(data: any) {
    const trimmed = trimToDepth(data, 2);
    expect(trimmed.item6.text).toBe(data.item6.text);
    expect(trimmed.item1).toBeDefined();
    expect(trimmed.item1.name).toEqual(data.item1.name);
    expect(trimmed.item1.item2).toBeDefined();
    expect(trimmed.item1.item2.name).toEqual(data.item1.item2.name);
    expect(trimmed.item5.item2).toBeDefined();
    expect(trimmed.item5.item2.name).toEqual(data.item5.item2.name);
    expect(Object.keys(trimmed.item1.item2.item3).length).toEqual(1);
    expect(trimmed.item1.item2.item3.href).toEqual(`/root/items/${data.item1.item2.item3.name}`);
    expect(trimmed.item5.item2.item3).toBeUndefined();
}

test("trimToDepth trims to depth 0.", () => testDepth0(trimData()) )

test("trimToDepth trims to depth 1.", () => testDepth1(trimData()) )

test("trimToDepth trims to depth 2.", () => testDepth2(trimData()) )

test("trimToDepth can trim an array.", () => {
  const data = [
    trimData(),
    trimData(),
    trimData()
  ]

  const trimmed = trimToDepth(data, 0);
  expect(Array.isArray(trimmed)).toBeTruthy()
  expect(trimmed.length).toEqual(data.length)
  data.forEach( (d_i, i) => testDepth0TrimmedData(trimmed[i], d_i) )
})

class RootClass {
  name: string = "root"
  item1: Item1
  item5: Item5
  item6: any = { text: "Not part of the hierarchy" }
}

@hierarchical({path:"/root/item1s"})
class Item1 {
  name: string = "item 1"
  item2: Item2
  nullItem: any = null
  undefinedItem: any = undefined
  falseItem: boolean = false
  trueItem: boolean = true
  numberItem: number = 1.2
}

@hierarchical({path:"/root/items"})
class Item2 {
  name: string = "item 2"
  item3: Item3
}

@hierarchical({path:"/root/items"})
class Item3 {
  name: string = "item 3"
  item4: Item4
}

@hierarchical({path:"/root/items"})
class Item4 {
  name: string = "item 4"
}

@hierarchical({path:"/root/item5s"})
class Item5 {
  name: string = "item 5"
  item2: Item2
}

function makeRootInstance() {
  const instance = new RootClass()
  instance.item1 = new Item1()
  instance.item1.item2 = new Item2()
  instance.item1.item2.item3 = new Item3()
  instance.item1.item2.item3.item4 = new Item4()

  instance.item5 = new Item5()
  instance.item5.item2 = new Item2()

  return instance
}

test("Can inject paths with decorators.", () => {
  const data = makeRootInstance()

  testDepth0(data)
  testDepth1(data)
  testDepth2(data)
})

test("nameOf extracts name and __path.", () => {
  const data = {
    name: 'test name',
    __path: '/test/path',
    other: 123
  }

  const named:any = nameOf(data)

  expect(named.name).toEqual(data.name)
  expect(named.__path).toEqual(data.__path)
  expect(named.other).toBeUndefined()
})

test("makeServiceObject includes links to all children", async () => {
  const oasDocData:any = await fs.promises.readFile(path.join(__dirname,'openapi.json'))
  const oasDoc = JSON.parse(oasDocData.toString())
  const service = makeServiceObject(oasDoc, 'weather')

  expect(service).toBeDefined()
  expect(service.links).toBeDefined()
  expect(Object.keys(service.links).length).toEqual(3)

  expect(service.links.self).toBeDefined()
  expect(service.links.temperature).toBeDefined()
  expect(service.links.humidity).toBeDefined()

  expect(service.links.self.href).toEqual("/weather")
  expect(service.links.temperature.href).toEqual("/weather/temperature")
  expect(service.links.humidity.href).toEqual("/weather/humidity")

  expect(service.links.temperature.types).toBeDefined()

  expect(service.links.temperature.types.length).toEqual(1)

  expect(service.links.temperature.types[0].uri).toEqual("http://ellipsistechnology.com/schemas/blackbox/")
  expect(service.links.temperature.types[0].name).toEqual("temperature")
})

test("makeServiceObject can create root service.", async () => {
  const oasDocData:any = await fs.promises.readFile(path.join(__dirname,'openapi.json'))
  const oasDoc = JSON.parse(oasDocData.toString())
  const service = makeServiceObject(oasDoc, '')

  expect(service).toBeDefined()
  expect(service.links).toBeDefined()
  expect(Object.keys(service.links).length).toEqual(3)

  expect(service.links.self).toBeDefined()
  expect(service.links.rulebase).toBeDefined()
  expect(service.links.weather).toBeDefined()

  expect(service.links.self.href).toEqual("/")
  expect(service.links.rulebase.href).toEqual("/rulebase")
  expect(service.links.weather.href).toEqual("/weather")

  expect(service.links.rulebase.types).toBeDefined()
  expect(service.links.weather.types).toBeDefined()

  expect(service.links.rulebase.types.length).toEqual(3)
  expect(service.links.weather.types.length).toEqual(1)

  expect(service.links.rulebase.types[0].uri).toEqual("http://ellipsistechnology.com/schemas/blackbox/")
  expect(service.links.rulebase.types[0].name).toEqual("rule")
  expect(service.links.rulebase.types[1].uri).toEqual("http://ellipsistechnology.com/schemas/blackbox/")
  expect(service.links.rulebase.types[1].name).toEqual("condition")
  expect(service.links.rulebase.types[2].uri).toEqual("http://ellipsistechnology.com/schemas/blackbox/")
  expect(service.links.rulebase.types[2].name).toEqual("value")

  expect(service.links.weather.types[0].uri).toEqual("http://ellipsistechnology.com/schemas/blackbox/")
  expect(service.links.weather.types[0].name).toEqual("temperature")
})

test("Can process parent and child paths.", async () => {
  const oasDocData:any = await fs.promises.readFile(path.join(__dirname,'openapi.json'))
  const oasDoc = JSON.parse(oasDocData.toString())

  const parents = findParentPaths(oasDoc)
  expect(parents).toBeDefined()
  expect(parents.length).toBeGreaterThan(0)

  parents.forEach((parentPath:string) => {

    const children = findChildren(oasDoc, parentPath)
    expect(children).toBeDefined()
    expect(children.length).toBeGreaterThan(0)

    children.forEach((childPath:string) => {
      expect(childPath.startsWith(parentPath)).toBeTruthy()
    })

  })
})
