export interface ServiceType {
    uri: string;
    name: string;
    format?: string;
    description?: string;
}
export interface ServiceLink extends Link {
    types?: ServiceType[];
}
export interface ServiceLinks {
    [key: string]: ServiceLink;
}
export interface Service {
    description: string;
    bbversion: string;
    links: ServiceLinks;
}
export interface NamedReference {
    name: string;
}
export interface Link {
    href: string;
    description?: string;
    [key: string]: Link | any;
}
export interface Links {
    self: Link;
    [key: string]: Link;
}
export interface VerboseObject {
    links?: Links;
}
export declare function nameOf(data: any): NamedReference;
export declare function verbose({ links }: {
    links: {
        [key: string]: Link;
    };
}): (target: any) => void;
export declare function verboseResponse<T>(target: T | T[], verbose?: boolean): (VerboseObject & T) | (VerboseObject & T)[];
export declare function verboseResponseObject<T>(target: T, verbose?: boolean): (VerboseObject & T);
export declare function verboseResponseArray<T>(targets: T[], verbose?: boolean): (VerboseObject & T)[];
export declare function trimToDepthArray(entity: any[], depth?: number): any[];
export declare function trimToDepth(entity: any, depth?: number): any;
export declare function identifiedBy({ id }: {
    id: string;
}): (target: any) => void;
export declare function hierarchical({ path }: {
    path: string;
}): (target: any) => void;
export declare function hasChildren(oasDoc: any, path: string): boolean;
export declare function findParentPaths(oasDoc: any): string[];
export declare function findChildren(oasDoc: any, rootServiceName: string): string[];
export declare function makeServiceObject(oasDoc: any, rootServiceName: string): Service;
