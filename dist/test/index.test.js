"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var __1 = require("..");
var fs = require("fs");
var path = require("path");
test("Verbose response includes verbose object only when verbose is true", function () {
    var data = {
        __path: "/self/path",
        test: "some test data",
        name: "test"
    };
    var newData1 = __1.verboseResponse(data, undefined);
    expect(newData1).toBe(data);
    expect(newData1.links).toBeUndefined();
    var newData2 = __1.verboseResponse(data, false);
    expect(newData2).toBe(data);
    expect(newData2.links).toBeUndefined();
    var newData3 = __1.verboseResponse(data, 'false');
    expect(newData3).toBe(data);
    expect(newData3.links).toBeUndefined();
    var newData4 = __1.verboseResponse(data, true);
    expect(newData4.links).toBeDefined();
    expect(newData4.test).toBe(data.test);
    var newData5 = __1.verboseResponse(data, 'true');
    expect(newData5.links).toBeDefined();
    expect(newData5.test).toBe(data.test);
    var newData6 = __1.verboseResponse(data, '');
    expect(newData6.links).toBeDefined();
    expect(newData6.test).toBe(data.test);
});
test("Verbose response includes correct path.", function () {
    var data = {
        __path: "/self/path",
        test: "some test data",
        name: "test"
    };
    var newData = __1.verboseResponse(data, true);
    expect(newData.links).toBeDefined();
    expect(newData.links.self).toBeDefined();
    expect(newData.links.self.href).toEqual('/self/path/test');
    expect(newData.test).toBe(data.test);
});
test("Verbose response operates on all elements of an array.", function () {
    var data = [
        { __path: "/self/path", name: "test1", test: "some test data" },
        { __path: "/self/path", name: "test2", test: 0 },
        { __path: "/self/path", name: "test3", test: false },
        { __path: "/self/path", name: "test4", test: "some ther test data" }
    ];
    var newData1 = __1.verboseResponseArray(data, undefined);
    newData1.forEach(function (d, i) { return expect(d).toBe(data[i]); });
    newData1.forEach(function (d) { return expect(d.links).toBeUndefined(); });
    var newData2 = __1.verboseResponseArray(data, false);
    newData2.forEach(function (d, i) { return expect(d).toBe(data[i]); });
    newData2.forEach(function (d) { return expect(d.links).toBeUndefined(); });
    var newData3 = __1.verboseResponseArray(data, 'false');
    newData3.forEach(function (d, i) { return expect(d).toBe(data[i]); });
    newData3.forEach(function (d) { return expect(d.links).toBeUndefined(); });
    var newData4 = __1.verboseResponseArray(data, true);
    newData4.forEach(function (d) { return expect(d.links).toBeDefined(); });
    newData4.forEach(function (d, i) { return expect(d.test).toBe(data[i].test); });
    var newData5 = __1.verboseResponseArray(data, 'true');
    newData5.forEach(function (d) { return expect(d.links).toBeDefined(); });
    newData5.forEach(function (d, i) { return expect(d.test).toBe(data[i].test); });
    var newData6 = __1.verboseResponseArray(data, '');
    newData6.forEach(function (d) { return expect(d.links).toBeDefined(); });
    newData6.forEach(function (d, i) { return expect(d.test).toBe(data[i].test); });
});
test("@verbose adds a link to verboseResponse.", function () {
    var Data = /** @class */ (function () {
        function Data() {
        }
        Data = __decorate([
            __1.verbose({ links: {
                    testLink: {
                        href: "/some/path",
                        description: "some description"
                    }
                } }),
            __1.hierarchical({ path: "/some/path" })
        ], Data);
        return Data;
    }());
    var data = new Data();
    var verboseData = __1.verboseResponseObject(data, true);
    expect(verboseData).toBeDefined();
    expect(verboseData.links).toBeDefined();
    expect(verboseData.links.testLink).toEqual({
        href: "/some/path",
        description: "some description"
    });
    expect(verboseData.links.self).toBeDefined();
});
function trimData() {
    return {
        name: "root",
        item1: {
            __path: '/root/item1s',
            name: "item 1",
            item2: {
                __path: '/root/items',
                name: "item 2",
                item3: {
                    __path: '/root/items',
                    name: "item 3",
                    item4: {
                        __path: '/root/items',
                        name: "item 4"
                    }
                }
            },
            nullItem: null,
            undefinedItem: undefined,
            falseItem: false,
            trueItem: true,
            numberItem: 1.2
        },
        item5: {
            __path: '/root/item5s',
            name: "item 5",
            item2: {
                __path: '/root/items',
                name: "item 2",
                item3: undefined
            }
        },
        item6: { text: "Not part of the hierarchy" }
    };
}
function testDepth0(data) {
    var trimmed = __1.trimToDepth(data, 0);
    testDepth0TrimmedData(trimmed, data);
}
function testDepth0TrimmedData(trimmed, data) {
    expect(trimmed.item6.text).toBe(data.item6.text);
    expect(Object.keys(trimmed.item1).length).toEqual(1);
    expect(trimmed.item1.href).toEqual("/root/item1s/" + data.item1.name);
    expect(Object.keys(trimmed.item5).length).toEqual(1);
    expect(trimmed.item5.href).toEqual("/root/item5s/" + data.item5.name);
}
function testDepth1(data) {
    var trimmed = __1.trimToDepth(data, 1);
    expect(trimmed.item6.text).toBe(data.item6.text);
    expect(trimmed.item1).toBeDefined();
    expect(trimmed.item1.name).toEqual(data.item1.name);
    expect(Object.keys(trimmed.item1.item2).length).toEqual(1);
    expect(trimmed.item1.item2.href).toEqual("/root/items/" + data.item1.item2.name);
    expect(Object.keys(trimmed.item5.item2).length).toEqual(1);
    expect(trimmed.item5.item2.href).toEqual("/root/items/" + data.item5.item2.name);
}
function testDepth2(data) {
    var trimmed = __1.trimToDepth(data, 2);
    expect(trimmed.item6.text).toBe(data.item6.text);
    expect(trimmed.item1).toBeDefined();
    expect(trimmed.item1.name).toEqual(data.item1.name);
    expect(trimmed.item1.item2).toBeDefined();
    expect(trimmed.item1.item2.name).toEqual(data.item1.item2.name);
    expect(trimmed.item5.item2).toBeDefined();
    expect(trimmed.item5.item2.name).toEqual(data.item5.item2.name);
    expect(Object.keys(trimmed.item1.item2.item3).length).toEqual(1);
    expect(trimmed.item1.item2.item3.href).toEqual("/root/items/" + data.item1.item2.item3.name);
    expect(trimmed.item5.item2.item3).toBeUndefined();
}
test("trimToDepth trims to depth 0.", function () { return testDepth0(trimData()); });
test("trimToDepth trims to depth 1.", function () { return testDepth1(trimData()); });
test("trimToDepth trims to depth 2.", function () { return testDepth2(trimData()); });
test("trimToDepth can trim an array.", function () {
    var data = [
        trimData(),
        trimData(),
        trimData()
    ];
    var trimmed = __1.trimToDepth(data, 0);
    expect(Array.isArray(trimmed)).toBeTruthy();
    expect(trimmed.length).toEqual(data.length);
    data.forEach(function (d_i, i) { return testDepth0TrimmedData(trimmed[i], d_i); });
});
var RootClass = /** @class */ (function () {
    function RootClass() {
        this.name = "root";
        this.item6 = { text: "Not part of the hierarchy" };
    }
    return RootClass;
}());
var Item1 = /** @class */ (function () {
    function Item1() {
        this.name = "item 1";
        this.nullItem = null;
        this.undefinedItem = undefined;
        this.falseItem = false;
        this.trueItem = true;
        this.numberItem = 1.2;
    }
    Item1 = __decorate([
        __1.hierarchical({ path: "/root/item1s" })
    ], Item1);
    return Item1;
}());
var Item2 = /** @class */ (function () {
    function Item2() {
        this.name = "item 2";
    }
    Item2 = __decorate([
        __1.hierarchical({ path: "/root/items" })
    ], Item2);
    return Item2;
}());
var Item3 = /** @class */ (function () {
    function Item3() {
        this.name = "item 3";
    }
    Item3 = __decorate([
        __1.hierarchical({ path: "/root/items" })
    ], Item3);
    return Item3;
}());
var Item4 = /** @class */ (function () {
    function Item4() {
        this.name = "item 4";
    }
    Item4 = __decorate([
        __1.hierarchical({ path: "/root/items" })
    ], Item4);
    return Item4;
}());
var Item5 = /** @class */ (function () {
    function Item5() {
        this.name = "item 5";
    }
    Item5 = __decorate([
        __1.hierarchical({ path: "/root/item5s" })
    ], Item5);
    return Item5;
}());
function makeRootInstance() {
    var instance = new RootClass();
    instance.item1 = new Item1();
    instance.item1.item2 = new Item2();
    instance.item1.item2.item3 = new Item3();
    instance.item1.item2.item3.item4 = new Item4();
    instance.item5 = new Item5();
    instance.item5.item2 = new Item2();
    return instance;
}
test("Can inject paths with decorators.", function () {
    var data = makeRootInstance();
    testDepth0(data);
    testDepth1(data);
    testDepth2(data);
});
test("nameOf extracts name and __path.", function () {
    var data = {
        name: 'test name',
        __path: '/test/path',
        other: 123
    };
    var named = __1.nameOf(data);
    expect(named.name).toEqual(data.name);
    expect(named.__path).toEqual(data.__path);
    expect(named.other).toBeUndefined();
});
test("makeServiceObject includes links to all children", function () { return __awaiter(void 0, void 0, void 0, function () {
    var oasDocData, oasDoc, service;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, fs.promises.readFile(path.join(__dirname, 'openapi.json'))];
            case 1:
                oasDocData = _a.sent();
                oasDoc = JSON.parse(oasDocData.toString());
                service = __1.makeServiceObject(oasDoc, 'weather');
                expect(service).toBeDefined();
                expect(service.links).toBeDefined();
                expect(Object.keys(service.links).length).toEqual(3);
                expect(service.links.self).toBeDefined();
                expect(service.links.temperature).toBeDefined();
                expect(service.links.humidity).toBeDefined();
                expect(service.links.self.href).toEqual("/weather");
                expect(service.links.temperature.href).toEqual("/weather/temperature");
                expect(service.links.humidity.href).toEqual("/weather/humidity");
                expect(service.links.temperature.types).toBeDefined();
                expect(service.links.temperature.types.length).toEqual(1);
                expect(service.links.temperature.types[0].uri).toEqual("http://ellipsistechnology.com/schemas/blackbox/");
                expect(service.links.temperature.types[0].name).toEqual("temperature");
                return [2 /*return*/];
        }
    });
}); });
test("makeServiceObject can create root service.", function () { return __awaiter(void 0, void 0, void 0, function () {
    var oasDocData, oasDoc, service;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, fs.promises.readFile(path.join(__dirname, 'openapi.json'))];
            case 1:
                oasDocData = _a.sent();
                oasDoc = JSON.parse(oasDocData.toString());
                service = __1.makeServiceObject(oasDoc, '');
                expect(service).toBeDefined();
                expect(service.links).toBeDefined();
                expect(Object.keys(service.links).length).toEqual(3);
                expect(service.links.self).toBeDefined();
                expect(service.links.rulebase).toBeDefined();
                expect(service.links.weather).toBeDefined();
                expect(service.links.self.href).toEqual("/");
                expect(service.links.rulebase.href).toEqual("/rulebase");
                expect(service.links.weather.href).toEqual("/weather");
                expect(service.links.rulebase.types).toBeDefined();
                expect(service.links.weather.types).toBeDefined();
                expect(service.links.rulebase.types.length).toEqual(3);
                expect(service.links.weather.types.length).toEqual(1);
                expect(service.links.rulebase.types[0].uri).toEqual("http://ellipsistechnology.com/schemas/blackbox/");
                expect(service.links.rulebase.types[0].name).toEqual("rule");
                expect(service.links.rulebase.types[1].uri).toEqual("http://ellipsistechnology.com/schemas/blackbox/");
                expect(service.links.rulebase.types[1].name).toEqual("condition");
                expect(service.links.rulebase.types[2].uri).toEqual("http://ellipsistechnology.com/schemas/blackbox/");
                expect(service.links.rulebase.types[2].name).toEqual("value");
                expect(service.links.weather.types[0].uri).toEqual("http://ellipsistechnology.com/schemas/blackbox/");
                expect(service.links.weather.types[0].name).toEqual("temperature");
                return [2 /*return*/];
        }
    });
}); });
test("Can process parent and child paths.", function () { return __awaiter(void 0, void 0, void 0, function () {
    var oasDocData, oasDoc, parents;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, fs.promises.readFile(path.join(__dirname, 'openapi.json'))];
            case 1:
                oasDocData = _a.sent();
                oasDoc = JSON.parse(oasDocData.toString());
                parents = __1.findParentPaths(oasDoc);
                expect(parents).toBeDefined();
                expect(parents.length).toBeGreaterThan(0);
                parents.forEach(function (parentPath) {
                    var children = __1.findChildren(oasDoc, parentPath);
                    expect(children).toBeDefined();
                    expect(children.length).toBeGreaterThan(0);
                    children.forEach(function (childPath) {
                        expect(childPath.startsWith(parentPath)).toBeTruthy();
                    });
                });
                return [2 /*return*/];
        }
    });
}); });
