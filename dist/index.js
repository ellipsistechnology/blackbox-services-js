"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.makeServiceObject = exports.findChildren = exports.findParentPaths = exports.hasChildren = exports.hierarchical = exports.identifiedBy = exports.trimToDepth = exports.trimToDepthArray = exports.verboseResponseArray = exports.verboseResponseObject = exports.verboseResponse = exports.verbose = exports.nameOf = void 0;
function nameOf(data) {
    return {
        name: data.__id ? data[data.__id] : data.name,
        __path: data.__path
    };
}
exports.nameOf = nameOf;
function convertBooleanParameter(val) {
    if (val === 'false')
        return false;
    if (val === '')
        return true;
    return val;
}
function verbose(_a) {
    var links = _a.links;
    return function (target) {
        if (!links)
            throw new Error("@verbose decorator is missing links");
        if (typeof target === 'function' && target.prototype)
            target.prototype.__links = links;
        else
            target.__links = links;
    };
}
exports.verbose = verbose;
function verboseResponse(target, verbose) {
    if (verbose === void 0) { verbose = false; }
    if (Array.isArray(target))
        return verboseResponseArray(target, verbose);
    else
        return verboseResponseObject(target, verbose);
}
exports.verboseResponse = verboseResponse;
function verboseResponseObject(target, verbose) {
    if (verbose === void 0) { verbose = false; }
    verbose = convertBooleanParameter(verbose);
    if (verbose && target.__path) {
        var v = Object.assign({ links: { self: { href: target.__path + "/" + (target.__id ? target[target.__id] : target.name) } } }, target);
        if (target.__links) {
            Object.assign(v.links, target.__links);
            delete v.__links;
        }
        return v;
    }
    else {
        return target;
    }
}
exports.verboseResponseObject = verboseResponseObject;
function verboseResponseArray(targets, verbose) {
    if (verbose === void 0) { verbose = false; }
    verbose = convertBooleanParameter(verbose);
    if (!verbose)
        return targets;
    return targets.map(function (target) { return verboseResponse(target, true); });
}
exports.verboseResponseArray = verboseResponseArray;
function trimToDepthArray(entity, depth) {
    if (depth === void 0) { depth = 0; }
    return entity.map(function (e) { return trimToDepth(e, depth); });
}
exports.trimToDepthArray = trimToDepthArray;
function trimToDepth(entity, depth) {
    if (depth === void 0) { depth = 0; }
    if (!entity || typeof entity !== 'object')
        return entity;
    if (Array.isArray(entity))
        return trimToDepthArray(entity, depth);
    var trimmed = {};
    var id = entity.__id ? entity.__id : 'name';
    if (!entity)
        return entity;
    Object.keys(entity).forEach(function (key) {
        // If the property is undefined or not hierarchical then just copy it:
        if (!entity[key] || !entity[key].__path) {
            trimmed[key] = entity[key];
        }
        // Recursively copy up to depth:
        else {
            if (!key.startsWith('__')) { // ignore config items starting with __
                // At max depth then insert link only:
                if (depth === 0) {
                    trimmed[key] = { href: entity[key].__path + "/" + entity[key][id] };
                }
                // Recurse on child hierarchy:
                else {
                    trimmed[key] = trimToDepth(entity[key], depth - 1);
                }
            }
        }
    });
    return trimmed;
}
exports.trimToDepth = trimToDepth;
function identifiedBy(_a) {
    var id = _a.id;
    return function (target) {
        if (!id)
            throw new Error("@identifiedBy decorator is missing id");
        if (typeof target === 'function' && target.prototype)
            target.prototype.__id = id;
        else
            target.__id = id;
    };
}
exports.identifiedBy = identifiedBy;
function hierarchical(_a) {
    var path = _a.path;
    return function (target) {
        if (!path)
            throw new Error("@hierarchical decorator is missing path for target " + (target.__id ? target[target.__id] : target.name));
        if (typeof target === 'function' && target.prototype)
            target.prototype.__path = path;
        else
            target.__path = path;
    };
}
exports.hierarchical = hierarchical;
function hasChildren(oasDoc, path) {
    var matcher = new RegExp("^" + path + "/.+");
    for (var _i = 0, _a = (Object.keys(oasDoc.paths)); _i < _a.length; _i++) {
        var p = _a[_i];
        if (matcher.test(p))
            return true;
    }
    return false;
}
exports.hasChildren = hasChildren;
function findParentPaths(oasDoc) {
    return Object.keys(oasDoc.paths)
        .filter(function (path) { return /^\/[^\/]+$/.test(path); }); // only one path component
    // .filter((path:string) => hasChildren(oasDoc, path)) // must be a parent
}
exports.findParentPaths = findParentPaths;
function findChildren(oasDoc, rootServiceName) {
    if (rootServiceName.startsWith('/'))
        rootServiceName = rootServiceName.substring(1);
    var matcher = rootServiceName === '' ? new RegExp("^/[^/]+$") : new RegExp("^/" + rootServiceName + "/[^/]+$");
    return Object.keys(oasDoc.paths)
        .map(function (path) { return path.endsWith('/') ? path.substring(0, path.length - 1) : path; }) // strip trailing slash
        .filter(function (path) { return matcher.test(path); });
}
exports.findChildren = findChildren;
function makeServiceObject(oasDoc, rootServiceName) {
    if (rootServiceName.startsWith('/'))
        rootServiceName = rootServiceName.substring(1);
    var self = "/" + rootServiceName;
    if (!oasDoc.paths[self])
        throw new Error("Path " + rootServiceName + " not found.");
    // Find immediate children:
    var children = findChildren(oasDoc, rootServiceName);
    if (children.length === 0)
        return undefined;
    // Create service object:
    var service = {
        description: oasDoc.paths["/" + rootServiceName] ? oasDoc.paths[self].get.summary : "",
        bbversion: "0.0.1",
        links: { self: { href: self } }
    };
    // Add links:
    children.forEach(function (child) {
        var name = /[^\/]+$/.exec(child)[0];
        var types = oasDoc.paths[child]['x-blackbox-types'];
        if (!types || types.length === 0) {
            types = findChildren(oasDoc, name).reduce(function (prev, current) {
                return oasDoc.paths[current]['x-blackbox-types'] ?
                    prev.concat(oasDoc.paths[current]['x-blackbox-types']) : prev;
            }, []);
        }
        service.links[name] = {
            href: child,
            description: oasDoc.paths[child].get.summary,
            types: types
        };
    });
    return service;
}
exports.makeServiceObject = makeServiceObject;
